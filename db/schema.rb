# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150507182759) do

  create_table "loans", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "publication_id"
    t.datetime "start_date"
    t.datetime "end_date"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  add_index "loans", ["publication_id"], name: "index_loans_on_publication_id"
  add_index "loans", ["user_id"], name: "index_loans_on_user_id"

  create_table "publications", force: :cascade do |t|
    t.string   "isbn"
    t.string   "title"
    t.string   "author"
    t.integer  "year"
    t.string   "type"
    t.string   "available"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "name"
    t.string   "password"
    t.string   "role"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "wishes"
  end

  create_table "wishes", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "publication_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  add_index "wishes", ["publication_id"], name: "index_wishes_on_publication_id"
  add_index "wishes", ["user_id"], name: "index_wishes_on_user_id"

end
