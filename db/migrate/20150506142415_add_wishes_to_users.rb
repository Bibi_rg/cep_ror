class AddWishesToUsers < ActiveRecord::Migration
  def change
    add_column :users, :wishes, :string
  end
end
