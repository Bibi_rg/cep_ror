class CreatePublications < ActiveRecord::Migration
  def change
    create_table :publications do |t|
      t.string :isbn
      t.string :title
      t.string :author
      t.integer :year
      t.string :type
      t.string :available

      t.timestamps null: false
    end
  end
end
