class User < ActiveRecord::Base
	serialize :wishes, Array
	has_many :loans
	has_many :wishes
end
