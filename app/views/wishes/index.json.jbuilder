json.array!(@wishes) do |wish|
  json.extract! wish, :id, :user_id, :publication_id
  json.url wish_url(wish, format: :json)
end
