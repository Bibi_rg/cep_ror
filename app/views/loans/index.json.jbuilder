json.array!(@loans) do |loan|
  json.extract! loan, :id, :user_id, :publication_id, :start_date, :end_date
  json.url loan_url(loan, format: :json)
end
