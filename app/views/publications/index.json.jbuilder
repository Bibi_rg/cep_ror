json.array!(@publications) do |publication|
  json.extract! publication, :id, :isbn, :title, :author, :year, :type, :available
  json.url publication_url(publication, format: :json)
end
