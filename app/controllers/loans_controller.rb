class LoansController < ApplicationController
  before_action :set_loan, only: [:show, :edit, :update, :destroy, :bookreturning, :bookreturn]

  # GET /loans
  # GET /loans.json
  def index
    if (session[:role] == 'admin') or (session[:role] == 'manager')
      @loans = Loan.all
    elsif session[:role] == 'reader'
      @user = User.find_by name: session[:user]
      @loans = Loan.where(user_id: @user.id)
    end
  end

  # GET /loans/1
  # GET /loans/1.json
  def show
  end

  # GET /loans/new
  def new
    @loan = Loan.new
  end

  # GET /loans/1/edit
  def edit
  end

  # POST /loans
  # POST /loans.json
  def create
    @loan = Loan.new(loan_params)

    respond_to do |format|
      @publication = Publication.find_by id: @loan.publication_id
      if @publication.available=='yes'
        if @publication.type=='physical'
          @publication.available='no'
          @publication.save
        end
        if @loan.save
          format.html { redirect_to @loan, notice: 'Loan was successfully created.' }
          format.json { render :show, status: :created, location: @loan }
        else
          format.html { render :new }
          format.json { render json: @loan.errors, status: :unprocessable_entity }
        end
        
      else
        format.html { redirect_to loans_path, notice: 'This publication is not available.' }
      end
    end
  end

  # PATCH/PUT /loans/1
  # PATCH/PUT /loans/1.json
  def update
    respond_to do |format|
      if @loan.update(loan_params)
        format.html { redirect_to @loan, notice: 'Loan was successfully updated.' }
        format.json { render :show, status: :ok, location: @loan }
      else
        format.html { render :edit }
        format.json { render json: @loan.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /loans/1
  # DELETE /loans/1.json
  def destroy
    @publication = Publication.find_by id: @loan.publication_id
    if @publication.available=='no'
      @publication.available='yes'
      @publication.save
    end
    @loan.destroy
    respond_to do |format|
      format.html { redirect_to loans_url, notice: 'Loan was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  
  def bookreturn
  end
  
  def bookreturning
    respond_to do |format|
      @publication = Publication.find_by id: @loan.publication_id
      if @publication.available=='no'
        @publication.available='yes'
        @publication.save
      end
      
      if @loan.update(loan_params)
        format.html { redirect_to @loan, notice: 'Loan was successfully returned.' }
        format.json { render :show, status: :ok, location: @loan }
      else
        format.html { render :bookreturn }
        format.json { render json: @loan.errors, status: :unprocessable_entity }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_loan
      @loan = Loan.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def loan_params
      params.require(:loan).permit(:user_id, :publication_id, :start_date, :end_date)
    end
end
