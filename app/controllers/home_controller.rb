class HomeController < ApplicationController

	def index
		@_current_user = session[:user]
		@_current_role = session[:role]
		if !@_current_user
				
		else
			if @_current_role == "admin"
				#redirect_to "admin/home", :notice => "Logged in admin!"
				redirect_to :controller => 'home' , :action => 'admin' ,:notice => "Logged in admin!"
			elsif @_current_role == "manager"
				redirect_to :controller => 'home' , :action => 'manager' ,:notice => "Logged in manager!"
			elsif @_current_role == "reader"
				redirect_to :controller => 'home' , :action => 'reader' ,:notice => "Logged in reader!"
			end 
		end
	end

	def admin
	end

	def manager
	end

	def reader
	end

end