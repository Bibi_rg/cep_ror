class SessionController < ApplicationController
	def new
	end

	def create
		@user = User.find_by name:    params[:username]
		if !@user
			flash.now.alert = "username #{params[:username]} was invalid"
			render :new
		elsif @user.password == params[:password]
			session[:user] = @user.name
			session[:role] = @user.role
			#Aquí debo enrutar las diferentes vistas "redirect_to" admin, manager, reader
			if @user.role == "admin"
				#redirect_to "admin/home", :notice => "Logged in admin!"
				redirect_to :controller => 'home' , :action => 'admin' ,:notice => "Logged in admin!"
			elsif @user.role == "manager"
				redirect_to :controller => 'home' , :action => 'manager' ,:notice => "Logged in manager!"
			elsif @user.role == "reader"
				redirect_to :controller => 'home' , :action => 'reader' ,:notice => "Logged in reader!"
			else
				redirect_to root_url, :notice => "Logged in!"
			end 
		else
			flash.now.alert = "password was invalid"
			render :new
		end
	end
		
	def destroy
		session[:user] = nil
			redirect_to :root, :notice => "Logged out!"
	end
	
end
